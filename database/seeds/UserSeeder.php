<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('users')->insert([
		    ['name' => 'admin','email' => 'admin@admin.ru', 'password' => Hash::make('123456'),'created_at' => Carbon::now(),'updated_at' => Carbon::now()],
	    ]);
    }
}
