<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCosToVersionTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('versions', function (Blueprint $table) {
	        $table->string('bundle_id',255)->after('name');
	        $table->string('last_version',255)->after('bundle_id');
	        $table->string('app_link',255)->after('last_version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('versions', function (Blueprint $table) {
            $table->dropColumn('bundle_id');
            $table->dropColumn('last_version');
            $table->dropColumn('app_link');
        });
    }
}
