<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('versions', function (Blueprint $table) {
          $table->string('link_iTunes')->index()->after('link_ios')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('versions', function (Blueprint $table) {
          $table->dropColumn('link_iTunes');
      });
    }
}
