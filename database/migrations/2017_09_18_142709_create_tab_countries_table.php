<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabCountriesTable extends Migration
{
    /**countries
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nulllable;
            $table->string('tab_id', 255)->nulllable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_countries');
    }
}
