<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('version_id')->index();
	        $table->foreign('version_id')->references('id')->on('versions')->onDelete('cascade');
            $table->string('param',100)->index();
            $table->string('value',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_versions');
    }
}
