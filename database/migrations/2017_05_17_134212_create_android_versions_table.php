<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAndroidVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('android_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('allow_country',255);
            $table->boolean('active')->default(0);
            $table->string('app_link',255);
            $table->string('last_version',50);
            $table->text('message_rus');
            $table->text('message_eng');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('android_versions');
    }
}
