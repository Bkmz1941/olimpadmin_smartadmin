<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Models\Image as Images;

trait UploadThumb
{

	public function setThumb($request, $obj_type, $model, $img_type){

		$image = $this->saveImage($request, $obj_type, $model, $img_type);

		return response()->json(['status' => true, 'value' => $image]);
	}

	protected function saveImage($file, $obj_type, $model, $img_type){

		$file = $this->saveThumb($file, $obj_type, $model, $img_type);

		if(File::exists($file->path.$file->name.'.sm.'.$file->extension)){
			$image = asset($file->path.$file->name.'.sm.'.$file->extension);
		}else{
			$image = asset($file->path.$file->name.'.'.$file->extension);
		}
		return $image;

	}

	protected function saveThumb($file, $obj_type, $model, $img_type){

		$extension = $file->getClientOriginalExtension();
		$path = "images/$obj_type/$img_type/$model->id/";
		$name = str_random(8);


		$thumb = $model->thumb($img_type)->first();

		if(is_null($thumb)) {

			$img = new Images();
			$img->path = $path;
			$img->name = $name;
			$img->extension = $extension;
			$img->type = $img_type;
			$model->thumb($img_type)->save($img);
		}else{
			$img = Images::find($thumb->id);
			$img->update([
				'path' => $path,
				'name' => $name,
				'extension' => $extension,
				'type' => $img_type
			]);
		}

		$this->cropThumb($path, $name, $extension, $file);

		return $img;
	}

	protected function cropThumb($path, $fileName, $fileExtension, $file){

		if(File::exists($path)){

			File::cleanDirectory($path);
		}else{
			File::makeDirectory($path, 0777, true);
		}

		$file->move($path,$fileName.'.'.$fileExtension);

		$image = Image::make($path.$fileName.'.'.$fileExtension);

		if ($image->width() >= 290 || $image->height() >= 290){

			$image->resize(null, 290, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			})->save($path.$fileName.'.sm.'.$fileExtension);
		}

		if ($image->width() > 100 || $image->height() > 70){

			$image->resize(50, 50, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			})->save($path.$fileName.'.xs.'.$fileExtension);
		}

		return true;

	}
}
