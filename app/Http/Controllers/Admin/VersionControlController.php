<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers;
use App\Http\Requests\VersionRequest;
use App\Http\Requests\VersionUpdateRequest;
use App\Models\AndroidVersion;
use App\Models\Build;
use App\Models\Country;
use App\Models\Country_version;
use App\Models\Image;
use App\Models\LinkVersion;
use App\Models\Params;
use App\Models\Version;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\Tab;
use App\Models\Tab_country;
use DB;

class VersionControlController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tabs_ajax_countries(Request $request) {

        $find_tab = DB::table('tab_countries')->where('tab_id', $request->name_tab)->get();
        return response()->json(['status' => true, 'db' => $find_tab]);
    }

    /**
     * @param Request $request
     */

    public function tabs_ajax(Request $request) {

        $new_tab = Tab::create([
            'name' => $request->name_tabs,
        ]);

        $arr_num = $request->num_countries;

        foreach ($arr_num as $value) {
            $new_tab = Tab_country::create([
                'name' => $value['index'],
                'tab_id' => $request->name_tabs
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function key(Request $request, $id) {

        $params = Params::where('version_id', '=', $id)->get();

        if (count($params)) {
            return response()->json(['status' => true, 'params' => $params]);
        } else {
            return response()->json(['status' => false, 'massage' => 'данных нет']);
        }

    }

    public function index(Request $request)
    {
    	$versions = Version::paginate(15);
        $params = Params::paginate(15);
		$versionsAndroid = AndroidVersion::paginate(15);
    	$active = 'ios';
        $countries = Country::all();
    	if(isset($request->active)){
		    $active = $request->active;
	    }
	    $tabs = Tab::all();
        return view('admin.version.index',['tabs' => $tabs ,'params' => $params, 'versions' => $versions, 'countries' => $countries, 'versionsAndroid' => $versionsAndroid,'active' => $active]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $versions = Version::all();
	    $countries = Country::all();
	    return view('admin.version.ios.create',['versions' => $versions, 'countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VersionRequest $request)
    {

		$version = Version::create([
					'name' => $request->name,
					'bundle_id' => $request->bundle_id,
					'last_version' => $request->last_version,
					'app_link' => isset($request->app_link)?$request->app_link:'',
					'api_url' => $request->api_url ? $request->api_url : '',
					'message_eng' => $request->message_eng,
					'message_rus' => $request->message_rus,
					'description' => $request->description,
					'disallow_payments' => $request->disallow_payments,
					'active' => isset($request->active) ? 1 : 0,
					'hidden' => isset($request->hidden) ? 1 : 0
				]);

		if(isset($request->allow_countries)){
			$version->countries()->attach($request->allow_countries);
		}

	    if(isset($request->param) && isset($request->value)){
            $collect = collect($request->param);
            $combine = $collect->combine($request->value);
            foreach ($combine as $param => $val){
	            $version->params()->create([
	            	'param' => $param,
	            	'value' => $val,
	            ]);
            }
	    }

	    if(isset($request->thumb)){
		    $version->setThumb($request->thumb, 'version', $version, 'thumb');
	    }

	    if ($request->input('copy_img', false)) {
	        $copy_version_id = $request->input('copy_img');
	        $image = Image::where('imageable_id', $copy_version_id)
                ->where('type', 'thumb')
                ->where('imageable_type', 'App\Models\Version')
                ->first();

	        if (isset($image->id)) {
	            $new_image = new Image();
	            $new_image->fill([
                    'path' => $image->path,
                    'name' => $image->name,
                    'extension' => $image->extension,
                    'type' => $image->type,
                    'imageable_id' => $version->id,
                    'imageable_type' => $image->imageable_type
                ]);
	            $new_image->save();
            }
        }

	    Session::flash('success','Версия успешно добавлена');
	    return redirect()->route('version.index',['active'=>'ios']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $version = Version::where('id',$id)->with('params','countries')->first();
        $versions = Version::where('id','<>',$id)->get();
	    $countries = Country::all();
        return view('admin.version.ios.edit', ['version' => $version, 'versions' => $versions, 'countries' => $countries]);
    }

    public function copyParams($id)
    {
	    $version = Version::find($id);
	    $params = $version->params()->get();
	    $countries = $version->countries()->get();
	    return response()->json(
	        [
	            'status' => true,
                'data' => [
                    'bundle_id' => $version->bundle_id,
                    'params' => $params,
                    'app_name' => $version->name,
                    'last_version' => $version->last_version,
                    'app_link' => $version->app_link,
                    'api_url' => $version->api_url ? $version->api_url : '',
                    'message_rus' => $version->message_rus,
                    'message_eng' => $version->message_eng,
                    'description' => $version->description,
                    'active' => $version->active,
                    'hidden' => $version->hidden,
                    'countries' => $countries,
                    'image' => Helpers::thumbSm($version, 'thumb'),
                ]
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VersionUpdateRequest $request, $id)
    {

	    $version = Version::find($id);
	    $version->update([
		    'name' => $request->name,
		    'bundle_id' => $request->bundle_id,
		    'last_version' => $request->last_version,
		    'app_link' => isset($request->app_link)?$request->app_link:'',
		    'api_url' => $request->api_url ? $request->api_url : '',
		    'message_eng' => $request->message_eng,
		    'message_rus' => $request->message_rus,
		    'description' => $request->description,
		    'disallow_payments' => $request->disallow_payments,
		    'active' => isset($request->active) ? 1 : 0,
		    'hidden' => isset($request->hidden) ? 1 : 0
	    ]);


	    if(isset($request->allow_countries2)){
		    $version->countries()->sync($request->allow_countries2);
	    } else {
            $version->countries()->sync($request->allow_countries2);
        }




	    $params = $version->params()->get();
        foreach ($params as $param){
	        $param->delete();
        }


	    if(isset($request->param2) && isset($request->value2)){
		    $collect = collect($request->param2);

		    $combine = $collect->combine($request->value2);
		    foreach ($combine as $param => $val){
			    $version->params()->create([
				    'param' => $param,
				    'value' => $val,
			    ]);
		    }
	    }


	    if(isset($request->thumb)){
		    $version->setThumb($request->thumb, 'version', $version, 'thumb');
	    }


	    Session::flash('success','Версия успешно обновлена');
	    return redirect()->route('version.index',['active'=>'ios']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $version = Version::find($id);
	    $version->delete();

	    return redirect()->route('version.index',['active'=>'ios']);
    }
}
