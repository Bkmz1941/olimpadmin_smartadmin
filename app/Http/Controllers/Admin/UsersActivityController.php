<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AndroidVersion;
use App\Models\Build;
use App\Models\User_param;
use App\Models\LinkVersion;
use App\Models\UsersActivity;
use App\Models\Version;
use Illuminate\Http\Request;


class UsersActivityController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $count_per_page = 10;
        $page = $request->input('page', 1);
        $offset = $page - 1;

        $user_param = User_param::paginate(10);
        $users_collection = UsersActivity::paginate(10);
        return view('admin.users_activity.index',['user_param' => $user_param, 'users' => $users_collection, 'active' => 'user_activity']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ajax(Request $request)
    {
        $count_per_page = $request->input('lenght', 10);
        $search = $request->input('search', false);
        $page = $request->input('drow', 1);
        $offset = $request->input('start', 0);
        $order = $request->input('order', [[ 'column'=>'0', 'dir' => 'asc' ]]);
        $field_map = ['id', 'login', 'versionApp', 'platform', 'created_at', 'updated_at'];

        if ($search && isset($search['value'])) {
            $collection = User_param::where(function ($query) use ($search) {
                    $query->where('login', 'like', $search['value'].'%')
                        ->orWhere('versionApp', 'like', $search['value'].'%')
                        ->orWhere('platform', 'like', $search['value'].'%')
                        ->orWhere('created_at', 'like', $search['value'].'%')
                        ->orWhere('updated_at', 'like', $search['value'].'%');
                })
                ->offset($offset)
                ->limit($count_per_page)
                ->orderBy($field_map[$order[0]['column']], $order[0]['dir'])
                ->get();
        } else {
            $collection = User_param::offset($offset)->limit($count_per_page)->orderBy($field_map[$order[0]['column']], $order[0]['dir'])->get();
        }
        $total_count = User_param::count();


        $response = [
            'drow' => $page,
            'data' => $collection,
            'recordsFiltered' => $total_count,
            'recordsTotal' => $total_count,
        ];

        return response()->json($response);
    }

    public function loadUsers(Request $request)
    {
        $users_arr = [];
        $sidx = $request->input('sidx', false);
        $sord = $request->input('sord', false);
        $rows = $request->input('rows', false);
        $page = $request->input('page', false);

        if (!$sidx || $sidx == '') {
            $users_collection = UsersActivity::all();
        } else {
            $users_collection = UsersActivity::orderBy($sidx, $sord)->get();
        }

        foreach ($users_collection as $user) {
            array_push($users_arr, [
                'user_id' => $user->user_id,
                'last_activity' => $user->last_activity,
                'email' => $user->email,
                'login' => $user->login ,
                'phone' => $user->phone,
            ]);
        }

        return response()->json([
              "rows" => $users_arr,
              "total" => 1,
              "page" => 1,
              "records" => 1,
        ]);
    }
}
