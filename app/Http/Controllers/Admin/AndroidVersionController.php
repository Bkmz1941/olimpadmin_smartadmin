<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AndroidVersionRequest;
use App\Models\AndroidVersion;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AndroidVersionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $versions = AndroidVersion::all();
	    $countries = Country::all();
	    return view('admin.version.android.create',['versions' => $versions, 'countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AndroidVersionRequest $request)
    {
	    $version = AndroidVersion::create([
		    'name' => $request->name,
		    'last_version' => $request->last_version,
		    'app_link' => isset($request->app_link)?$request->app_link:'',
		    'allow_country' => $request->allow_country,
		    'message_eng' => $request->message_eng,
		    'message_rus' => $request->message_rus,
		    'description' => $request->description,
		    'active' => isset($request->active) ? 1 : 0,
		    'hidden' => isset($request->hidden) ? 1 : 0
	    ]);

	    if(isset($request->allow_countries)){
		    $version->countries()->attach($request->allow_countries);
	    }


	    if(isset($request->param) && isset($request->value)){
		    $collect = collect($request->param);
		    $combine = $collect->combine($request->value);
		    foreach ($combine as $param => $val){
			    $version->params()->create([
				    'param' => $param,
				    'value' => $val,
			    ]);
		    }
	    }

	    if(isset($request->thumb)){
		    $version->setThumb($request->thumb, 'androidVersion', $version, 'thumb');
	    }

	    Session::flash('success','Android версия успешно добавлена');
	    return redirect()->route('version.index',['active' => 'android']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $version = AndroidVersion::where('id',$id)->with('params','countries')->first();
	    $versions = AndroidVersion::where('id','<>',$id)->get();
	    $countries = Country::all();
	    return view('admin.version.android.edit', ['version' => $version, 'versions' => $versions, 'countries' => $countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AndroidVersionRequest $request, $id)
    {
	    $version = AndroidVersion::find($id);
	    $version->update([
		    'name' => $request->name,
		    'last_version' => $request->last_version,
		    'app_link' => isset($request->app_link)?$request->app_link:'',
		    'allow_country' => $request->allow_country,
		    'message_eng' => $request->message_eng,
		    'message_rus' => $request->message_rus,
		    'description' => $request->description,
		    'active' => isset($request->active) ? 1 : 0,
		    'hidden' => isset($request->hidden) ? 1 : 0
	    ]);

	    if(isset($request->allow_countries)){
		    $version->countries()->sync($request->allow_countries);
	    }

	    $params = $version->params()->get();
	    foreach ($params as $param){
		    $param->delete();
	    }

	    if(isset($request->param) && isset($request->value)){
		    $collect = collect($request->param);
		    $combine = $collect->combine($request->value);
		    foreach ($combine as $param => $val){
			    $version->params()->create([
				    'param' => $param,
				    'value' => $val,
			    ]);
		    }
	    }


	    if(isset($request->thumb)){
		    $version->setThumb($request->thumb, 'version', $version, 'thumb');
	    }


	    Session::flash('success','Android версия успешно обновлена');
	    return redirect()->route('version.index',['active' => 'android']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $version = AndroidVersion::find($id);
	    $version->delete();

	    Session::flash('success','Android версия успешно удалена');
	    return redirect()->route('version.index',['active' => 'android']);
    }
}
