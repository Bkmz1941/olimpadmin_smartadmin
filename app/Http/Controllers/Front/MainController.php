<?php

namespace App\Http\Controllers\Front;

use App\Models\AndroidVersion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Version;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class MainController extends Controller
{
    public function index(Request $request)
    {
        $versions = Version::where('hidden',0)->paginate(8);
	    $androidVersions = $this->getAndroidLink();
        //$androidVersions = AndroidVersion::where('hidden',0)->paginate(8);
	    $active = 'ios';
	    if(isset($request->active)){
		    $active = $request->active;
	    }

	    $secret_parameter = '7ba2ff57702c100155220398731e9504gdms0524';
	    $check_param = $request->input('secret_key', false);

	    if ($check_param && $secret_parameter == $check_param) {
            return view('front.main.index', ['versions' => $versions, 'androidVersions' => $androidVersions, 'active' => $active]);
        } else {
            return view('front.main.empty', ['versions' => $versions, 'androidVersions' => $androidVersions, 'active' => $active]);
        }
    }

    public function ios()
    {
	    $versions = Version::where('hidden',0)->paginate(8);
	    return view('front.main.ios',['versions' => $versions]);
    }

	public function android()
	{
		$versions = AndroidVersion::where('hidden',0)->paginate(8);
		return view('front.main.android',['versions' => $versions]);
	}

	protected function getAndroidLink()
	{
		$client = new Client();
		$res = $client->request('GET', 'http://olimpcasino.bid/last/info/');
		$data = json_decode($res->getBody(),true);
		foreach ($data['links'] as &$link){
			$link = base64_decode($link);
		}
		return $data;
	}
}
