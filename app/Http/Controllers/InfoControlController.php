<?php

namespace App\Http\Controllers;

use App\Models\Version;
use App\Models\Image;


class InfoControlController extends Controller
{
    public function index() {

      $versions = Version::all();
      $images = Image::all();
      return view('info.index',['versions' => $versions, 'images' => $images]);

    }
}
