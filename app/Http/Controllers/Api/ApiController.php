<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use App\Models\UsersActivity;
use App\Models\Version;
use App\Models\User_param;
use App\Presenters\Version\ApiPresenter;
use Illuminate\Http\Request;





class ApiController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function param(Request $request) {

        $login = $request->input('login', false);
        $platform = $request->input('platform', false);
        $versionApp = $request->input('versionApp', false);
        $current_time = Carbon::now();

        if ($login && $versionApp && $platform) {
            $check_login = User_param::where('login', '=', $login)->first();
            if (isset($check_login->id)) {
                $check_login->login = $login;
                $check_login->platform = $platform;
                $check_login->versionApp = $versionApp;
                $check_login->updated_at = $current_time;
                $check_login->save();
            } else {
                $new_user_params = new User_param();
                $new_user_params->login = $login;
                $new_user_params->platform = $platform;
                $new_user_params->versionApp = $versionApp;
                $new_user_params->save();
            }
            return response()->json(['status' => true, 'message' => 'Success']);
        } else {
            return response()->json(['status' => false, 'message' => 'There are no parameters']);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function version()
    {
        $versions = Version::with('params','countries')->where('active',1)->get();

        if(count($versions)) {

            $datas = [];

            foreach ($versions as $version) {
                $pressenter = new ApiPresenter($version);

                $datas[] = [
                    'name' => $pressenter->name,
                    'bundle_id' => $pressenter->bundle_id,
                    'last_version' => $pressenter->last_version,
                    'app_link' => $pressenter->app_link,
                    'api_url' => $pressenter->api_url,
                    'message_rus' => $pressenter->message_rus,
                    'message_eng' => $pressenter->message_eng,
                    'allow_countries' => $pressenter->allowCountries(),
                    'disallow_payments' => $pressenter->disalowPayment(),
                    'params' => $pressenter->parameters()
                ];
            }

            return response()->json(['status' => true,'data' => $datas]);
        }else{
            return response()->json(['status' => false, 'message' => 'Нет по данных']);
        }
    }

    /**
     * Обновить активность юзера
     * параметры: user_id, login, email, phone
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateActivity(Request $request) {
        $user_id = $request->input('user_id', false);
        $status = false;
        $message = '';

        if ($user_id) {

            $check_user = UsersActivity::where('user_id', $user_id)->first();
            if (isset($check_user->id)) {
                $check_user->last_activity = date('Y-m-d H:i:s');
                $check_user->save();
            } else {
                $login = $request->input('login', false);
                $email = $request->input('email', false);
                $phone = $request->input('phone', false);

                $newUserActivity = new UsersActivity();
                $newUserActivity->fill([
                    'user_id' => $user_id,
                    'last_activity' => date('Y-m-d H:i:s'),
                ]);

                if ($login) {
                    $newUserActivity->login = $login;
                }
                if ($email) {
                    $newUserActivity->email = $email;
                }
                if ($phone) {
                    $newUserActivity->phone = $phone;
                }

                $newUserActivity->save();
            }
            $status = true;
            $message = 'обновлено';

        } else {
            $status = false;
            $message = 'user_id обязательное поле';
        }

        return response()->json(['status' => $status, 'message' => $message]);
    }

    public function versionByBundleId($id)
    {
        $version = Version::with('params')->where('bundle_id', $id)->orderBy('id','DESC')->first();

        $pressenter = new ApiPresenter($version);

        if($pressenter->countElems()){

            return response()->json([
                'status' => true,
                'details_link' => $pressenter->detailsLink(),
                'data' => [
                    'name' => $pressenter->name,
                    'last_version' => $pressenter->last_version,
                    'app_link' => $pressenter->app_link,
                    'api_url' => $pressenter->api_url,
                    'message_rus' => $pressenter->message_rus,
                    'message_eng' => $pressenter->message_eng,
                    'active' => $pressenter->isActive(),
                    'allow_countries' => $pressenter->allowCountries(),
                    'disallow_payments' => $pressenter->disalowPayment(),
                    'params' => $pressenter->parameters()
                ]
            ]);
        }else{
            return response()->json(['status' => false, 'message' => 'Нет по данных по этому bundle_id']);
        }
    }

}
