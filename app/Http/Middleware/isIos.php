<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;

class isIos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $agent = new Agent();

	    if($agent->is('OS X')){
		    return $next($request);
	    }elseif ($agent->isAndroidOS()){
		    return response()->redirectToRoute('main.android');
	    }else{
		    return response()->redirectToRoute('main');
	    }

    }
}
