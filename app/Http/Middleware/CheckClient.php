<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;


class CheckClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $agent = new Agent();

	    if($agent->is('OS X')){
			//return response()->redirectToRoute('main.ios');
		    $request->active = 'ios';
	    }elseif ($agent->isAndroidOS()){
		    //return response()->redirectToRoute('main.android');
		    $request->active = 'android';
	    }else{
		    $request->active = 'ios';
	    }

	    return $next($request);
    }
}
