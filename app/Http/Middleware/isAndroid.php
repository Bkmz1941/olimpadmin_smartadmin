<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;

class isAndroid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $agent = new Agent();

	    if($agent->is('OS X')){
		    return response()->redirectToRoute('main.ios');
	    }elseif ($agent->isAndroidOS()){
		    return $next($request);
	    }else{
		    return response()->redirectToRoute('main');
	    }
    }
}
