<?php
namespace App\Http;

use Illuminate\Support\Facades\File;

class Helpers {

	public static function normalizePhone($sPhone){

		return strtr($sPhone, array(' ' => '', '+' => '', '-' => '', '(' => '', ')' => ''));
	}

	public static function intPhone($sPhone){

		return preg_replace('/^(8)/', '+7', $sPhone);
	}

	public static function parentCategory($category, $breadcrumbs, $middle_breadcrumbs = []){


		if(!is_null($category->parentCategory)){
			$current_bread = [];

			$ancestor = $category->parentCategory;

			$current_bread['title'] = $ancestor->title;
			$current_bread['url'] = route('page.show', $ancestor->id);
			$current_bread['first'] = false;
			$current_bread['last'] = false;
			array_push($middle_breadcrumbs,(object)$current_bread);

			self::parentCategory($ancestor,$breadcrumbs, $middle_breadcrumbs);
		}else{
			foreach (array_reverse($middle_breadcrumbs) as $middle){

				$breadcrumbs->push($middle->title, $middle->url);
			}
			return $breadcrumbs;
		}
		return $breadcrumbs;
	}

	public static function thumbSm($obj, $type = null){

		if(!is_null($type)){
			if(!is_null($obj->thumb($type)->first())){

				$img = $obj->thumb($type)->first();

				if(File::exists($img->path.$img->name.'.sm.'.$img->extension)){
					return asset($img->path.$img->name.'.sm.'.$img->extension);
				}else{
					return asset($img->path.$img->name.'.'.$img->extension);
				}
			}else{
				return asset('img/noimage.png');
			}
		}
	}

	public static function thumbFull($obj, $type = null){

		if(!is_null($type)){
			if(!is_null($obj->thumb($type)->first())){

				$img = $obj->thumb($type)->first();

				if(File::exists($img->path.$img->name.'.'.$img->extension)){
					return asset($img->path.$img->name.'.'.$img->extension);
				}else{
					return asset($img->path.$img->name.'.'.$img->extension);
				}
			}else{
				return asset('img/noimage.png');
			}
		}
	}

	public static function calculateBalance($user, $sum)
	{
		$oldBalance = $user->balance;
		$newBalance = $sum;

		if($oldBalance > $newBalance){

			$total = -($oldBalance - $newBalance);
			$comment = 'Администратор уменьшил баланс';
		}else{

			$total = $newBalance - $oldBalance;
			$comment = 'Администратор увеличил баланс';
		}

		$data['sum'] = $total;
		$data['comment'] = $comment;

		return $data;
	}
}
