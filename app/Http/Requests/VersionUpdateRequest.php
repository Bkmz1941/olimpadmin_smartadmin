<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class VersionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'name' => 'required|min:3|max:100',
	        'active' => 'sometimes|required|in:on',
	        'hidden' => 'sometimes|required|in:on',
	        'thumb' => 'sometimes|required|mimes:jpeg,png',
	        'bundle_id' => 'required|min:3|max:255',
	        'last_version' => 'required|min:3|max:255',
	        'app_link' => 'sometimes|required|min:3',
	        'api_url' => 'max:255',
	        'message_eng' => 'required|min:3|max:255',
	        'message_rus' => 'required|min:3|max:255',
            'description' => 'required|min:3',
        ];
    }
}
