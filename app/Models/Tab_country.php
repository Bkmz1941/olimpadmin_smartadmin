<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tab_country extends Model
{
    protected $fillable = ['name', 'tab_id'];

    public function tab_count()
    {
        return $this->belongsTo('App\Models\Tab');
    }

}
