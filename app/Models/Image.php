<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Image extends Model
{
	protected $fillable = ['path', 'name', 'extension', 'type', 'imageable_id', 'imageable_type'];

	public static function boot()
	{
		parent::boot();

		static::deleted(function($image){

			$path = $image->path;
			if(File::exists($path)){

				File::deleteDirectory($path);
			}

		});
	}

	public function imageable()
	{
		return $this->morphTo();
	}
}
