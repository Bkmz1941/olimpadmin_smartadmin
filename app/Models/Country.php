<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name','iso_code_2','iso_code_3'];

    public function versions()
    {
        return $this->belongsToMany(Version::class);
    }

    public function androidVersions()
    {
	    return $this->belongsToMany(AndroidVersion::class);
    }
}
