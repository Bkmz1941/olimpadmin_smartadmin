<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tab extends Model
{
    protected $fillable = ['name'];

    public function tab_function()
    {
        return $this->hasMany('App\Models\Tab_country');
    }
}
