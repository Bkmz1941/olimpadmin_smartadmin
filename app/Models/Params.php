<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Params extends Model
{
    protected $fillable = ['version_id', 'param', 'value'];

    public function version()
    {
    	return $this->belongsTo(Version::class);
    }

	public function androidVersion()
	{
		return $this->belongsTo(AndroidVersion::class,'id','version_id');
	}
}
