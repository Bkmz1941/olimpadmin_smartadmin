<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_param extends Model
{
    protected $fillable = [
        'login', 'versionApp', 'platform',
    ];
}
