<?php

namespace App\Models;

use App\Traits\UploadThumb;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Version extends Model
{
	use SoftDeletes, UploadThumb;

    protected $fillable = ['name', 'active', 'bundle_id', 'last_version', 'app_link','message_rus','message_eng', 'hidden', 'description', 'api_url', 'disallow_payments'];

	public static function boot()
	{
		parent::boot();

		static::deleted(function($version){

			$versions = $version->params()->get();
			$image = $version->thumb()->first();
			if(count($versions) > 0){
				foreach ($versions as $version){
					$version->delete();
				}
			}
			if($image){
				$image->delete();
			}
		});
	}

	public function thumb($type = null)
	{
		return $this->morphOne(Image::class, 'imageable');
	}

    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    public function params()
    {
    	return $this->hasMany(Params::class);
    }

    public function countries()
    {
    	return $this->belongsToMany(Country::class);
    }
}
