<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersActivity extends Model
{
    protected $table = 'users_activity';
    public $timestamps = false;
	protected $fillable = ['user_id', 'last_activity', 'login', 'email', 'phone'];

}
