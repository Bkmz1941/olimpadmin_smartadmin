<?php

namespace App\Models;

use App\Traits\UploadThumb;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AndroidVersion extends Model
{
	use SoftDeletes, UploadThumb;

	protected $fillable = ['name', 'active', 'app_link', 'last_version', 'message_rus', 'message_eng','hidden', 'description'];

	public static function boot()
	{
		parent::boot();

		static::deleted(function($version){

			$versions = $version->params()->get();
			$image = $version->thumb()->first();
			if(count($versions) > 0){
				foreach ($versions as $version){
					$version->delete();
				}
			}
			if($image){
				$image->delete();
			}
		});
	}

	public function thumb($type = null)
	{
		return $this->morphOne(Image::class, 'imageable');
	}

	public function image()
	{
		return $this->belongsTo('App\Models\Image');
	}

	public function params()
	{
		return $this->hasMany(Params::class,'version_id','id');
	}

	public function countries()
	{
		return $this->belongsToMany(Country::class);
	}
}
