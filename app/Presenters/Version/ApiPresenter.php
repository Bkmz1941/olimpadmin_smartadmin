<?php
/**
 * Created by PhpStorm.
 * User: mamau
 * Date: 15.05.17
 * Time: 18:44
 */

namespace App\Presenters\Version;


use App\Presenters\Presenter;

class ApiPresenter extends Presenter
{


	public function isActive()
	{
		return (bool) $this->model->active;
	}

	public function detailsLink()
	{
		return 'http://olimpapp.info';
	}

	public function parameters()
	{
		$params = $this->model->params()->select('param','value')->get();
		return $params->toArray();
	}

	public function allowCountries()
	{
		$countries = [];
		$params = $this->model->countries()->select('iso_code_2')->get();
		foreach ($params as $param){
			array_push($countries,$param->iso_code_2);
		}
		return $countries;
	}

	public function disalowPayment()
    {
        $diss = [];
        $params = $this->model->disallow_payments;

        if(!is_null($params)){
            $arr = explode(',',$params);
            foreach ($arr as $param){
                array_push($diss, strtolower(trim($param)));
            }
        }
        return $diss;
    }

	public function countElems()
	{
		return count($this->model);
	}
}
