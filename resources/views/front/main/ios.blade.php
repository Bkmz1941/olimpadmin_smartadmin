@extends('front.layouts.app')

@section('content')
    <div class="navbar navbar-default navbar-static-top header headbar">
        <div class="col-xs-12"><h5 class="center head"><strong>Приложение ОЛИМП IOS</strong></h5></div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-condensed">
                    <tbody>
                    @forelse($versions as $version)
                        @if($version->active)
                            <tr>
                                <td>
                                    <img class="img-responsive version_logo" src="{{Helpers::thumbSm($version, 'thumb')}}" alt="">
                                </td>
                                <td>
                                    <div class="version_name"><b>{{$version->name}}</b></div>
                                    <div class="version">{{$version->last_version}}</div>
                                </td>
                                <td >
                                    @if($version->active)
                                        <a class="version_link_iTunes uk-button uk-button-default uk-button-large" href="{{$version->app_link}}">Загрузить</a>
                                    @endif
                                </td>
                            </tr>
                        @else

                        @endif
                    @empty
                        <tr>
                            <td colspan="5" class="text-center">Версий нет</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

