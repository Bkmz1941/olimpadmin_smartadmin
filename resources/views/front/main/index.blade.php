@extends('front.layouts.app')

@section('content')
    <div class="navbar navbar-default navbar-static-top header headbar">
        <div class="col-xs-12"><h5 class="center head"><strong>Olimp Official Apps</strong></h5></div>
    </div>

    <div class="tabbable">
        <ul class="nav nav-tabs" id="myTab4">
            <li class="{{$active=='ios'?'active':''}}">
                <a data-toggle="tab" href="#ios" aria-expanded="true">
                    <i class="blue ace-icon fa fa-apple bigger-120"></i>
                    IOS
                </a>
            </li>

            <li class="{{$active=='android'?'active':''}}">
                <a data-toggle="tab" href="#android" aria-expanded="false">
                    <i class="green ace-icon fa fa-android bigger-120"></i>
                    Android
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="ios" class="tab-pane {{$active=='ios'?'active':''}}">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-condensed">
                                <tbody>
                                @forelse($versions as $version)
                                    @if($version->active)
                                        <tr>
                                            <td>
                                                <img class="img-responsive version_logo" src="{{Helpers::thumbSm($version, 'thumb')}}" alt="">
                                            </td>
                                            <td>
                                                <div class="version_name"><b>{{$version->name}}</b></div>
                                                <div class="version">{{$version->last_version}}</div>
                                                <div class="version">{{$version->description}}</div>
                                            </td>
                                            <td >
                                                @if($version->active)
                                                    <a class="version_link_iTunes uk-button uk-button-default uk-button-large" href="{{$version->app_link}}" >Загрузить</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @else

                                    @endif
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Версий нет</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="android" class="tab-pane {{$active=='android'?'active':''}}">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-condensed">
                                <tbody>
                                @if($androidVersions)

                                    <tr>
                                        <td>
                                            <img class="img-responsive version_logo" src={{asset("img/noimage.png")}} alt="">
                                        </td>
                                        <td>
                                            <div class="version_name"><b>Olimp APP</b></div>
                                            <div class="version">{{$androidVersions['android_name']}}</div>
                                        </td>
                                        <td>
                                            <a class="version_link_iTunes uk-button uk-button-default uk-button-large" href="{{$androidVersions['links'][0]}}" >Загрузить</a>

                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="5" class="text-center">Версий нет</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

