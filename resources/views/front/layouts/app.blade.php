<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>
        @section('title')
            {{ config('app.name', 'OlimpVC') }}
        @show
    </title>

    @section('styles')
        <link rel="stylesheet" href="{{ asset('css/front/app.min.css') }}">
    @show
</head>
<body class="no-skin">
@yield('content')
@section('scripts')
    <script src="{{asset('js/front/app.min.js')}}"></script>
@show
</body>
</html>

