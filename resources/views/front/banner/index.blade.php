<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>
        Test banner
    </title>

    <meta name="smartbanner:title" content="Olimp App">
    <meta name="smartbanner:author" content="Download our Official App">
    <meta name="smartbanner:price" content="FREE">
    <meta name="smartbanner:price-suffix-apple" content=" - On the App Store">
    <meta name="smartbanner:price-suffix-google" content=" - In Google Play">
    <meta name="smartbanner:icon-apple" content="https://olimpapp.info/img/logo_olimp.png">
    <meta name="smartbanner:icon-google" content="https://olimpapp.info/img/logo_olimp.png">
    <meta name="smartbanner:button" content="VIEW">
    <meta name="smartbanner:button-url-apple" content="https://olimpapp.info">
    <meta name="smartbanner:button-url-google" content="https://olimpapp.info">
    <meta name="smartbanner:enabled-platforms" content="android,ios">
    <link rel="stylesheet" href="https://olimpapp.info/js/smartbanner/dist/smartbanner.css">
    <script src="https://olimpapp.info/js/smartbanner/dist/smartbanner.js"></script>
</head>
<body>

    <h1>Hello Banner</h1>
</body>

</html>

