@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12">
      <table class="table">
        <tbody>
          <tr>
            @forelse($images as $image)
                  <td>{{$image->path}}</td>

          </tr>
            @empty
          <tr>
              <td colspan="5" class="text-center">нет версий</td>
          </tr>
            @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
@section('scripts')
    @parent
    <script>
        var thumb_img = '{{Helpers::thumbSm($version, 'thumb')}}';
    </script>
    <script src="{{asset('ace/assets/js/src/elements.fileinput.js')}}"></script>
    <script src="{{asset('js/version/edit.js')}}"></script>
@endsection
