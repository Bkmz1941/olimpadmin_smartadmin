@extends('admin.layouts.app')
@section('title')
    404 страница не найдена
@stop
@section('content')
    @include("admin.layouts._main.header")
    <div class="container main-container prl">
        <div class="container-d-line"></div>
        <div class="col-xs-12">
            <br />
            <div class="error-container">
                <div class="well">
                    <h1 class="grey lighter smaller">
										<span class="blue bigger-125">
											<i class="ace-icon fa fa-sitemap"></i>
											404
										</span>
                        Страница не найдена
                    </h1>

                    <hr>
                    <h3 class="lighter smaller">Мы посмотрели везде, но ничего не нашли!</h3>

                    <div>
                        <div class="space"></div>
                        <h4 class="smaller">Попробуйте сделать следущее:</h4>

                        <ul class="list-unstyled spaced inline bigger-110 margin-15">
                            <li>
                                <i class="ace-icon fa fa-hand-o-right blue"></i>
                                Проверьте адрес cтраницы на ошибки
                            </li>

                            <li>
                                <i class="ace-icon fa fa-hand-o-right blue"></i>
                                Сообщите нам
                            </li>
                        </ul>
                    </div>

                    <hr>
                    <div class="space"></div>

                    <div class="center">
                        <a class="btn btn-grey" href="javascript:history.back()">
                            <i class="ace-icon fa fa-arrow-left"></i>
                            Назад
                        </a>

                        <a class="btn btn-primary" href="{{url('/')}}">
                            <i class="ace-icon fa fa-building-o"></i>
                            На главную
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
