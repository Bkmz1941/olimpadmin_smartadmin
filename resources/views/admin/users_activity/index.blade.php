@extends('admin.layouts.app')

@section('title')
    Пользователи
@stop

@section('content')

    @include("admin.layouts._main.header")

            <div class="row">
                <div class="col-lg-12">
                    <div class="well well-sm well-light">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th data-hide="phone">ID</th>
                                <th>Login</th>
                                <th data-class="expand">versionApp</th>
                                <th data-hide="phone">platform</th>
                                <th data-hide="phone">Last activity</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

    <script src="{{ asset('custom/js/dataTableusers.js') }}"></script>

    @include("admin.layouts._main.footer")

@endsection

@section('scripts')
    @parent

            <script>
                loadScript(('/smartadmin/js/table/jquery.dataTables.min.js'), function(){
                    loadScript(("/smartadmin/js/table/dataTables.colVis.min.js"), function(){
                        loadScript(("/smartadmin/js/table/dataTables.tableTools.min.js"), function(){
                            loadScript(("/smartadmin/js/table/dataTables.bootstrap.min.js"), function(){
                                loadScript(("/smartadmin/js/table/datatables.responsive.min.js"), pagefunction)
                            });
                        });
                    });
                });
            </script>

@endsection
