<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta id="token" name="csrf-token" content="{{ csrf_token() }}">

    <title>

        @section('title')

        @show

    </title>

    @section('styles')

        <link rel="stylesheet" href="{{ asset('smartadmin/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/demo.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/invoice.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/lockscreen.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/smartadmin-production.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/smartadmin-production-plugins.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/smartadmin-rtl.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/smartadmin-skins.min.css') }}">
        <link rel="stylesheet" href="{{ asset('smartadmin/css/your_style.css') }}">
        <link rel="stylesheet" href="{{ asset('custom/duallistbox/bootstrap-duallistbox.css') }}">

    @show

</head>

    <body class="no-skin">

    @yield('content')

    @section('scripts')
        <script src="/jquery.js"></script>
        <script src="{{ asset('smartadmin/js/libs/jquery-ui-1.10.3.min.js') }}"></script>
        <script src="{{ asset('smartadmin/js/app.config.js') }}"></script>
        <script src="{{ asset('smartadmin/js/app.min.js') }}"></script>
        <script src="{{ asset('smartadmin/js/bootstrap/bootstrap.min.js' )}}"></script>
        <script src="{{ asset('custom/vue/vue.js') }}"></script>
        <script src="{{ asset('custom/duallistbox/jquery.bootstrap-duallistbox.js') }}"></script>

    @show

    </body>

</html>

