<header id="header">
    <div id="logo-group">
        <span id="logo"> <img src="/smartadmin/img/logo.png" alt="SmartAdmin"> </span>
    </div>
    @if (Auth::guest())
    @else
            <div id="left_menu" class="pull-left" style="margin-top: 10px;">
                <a class="btn btn-default {{(Request::path()=='version')?'active':''}}" href="{{route('version.index')}}">Версии</a>
                <a class="btn btn-default {{(Request::path()=='users-activity')?'active':''}}" href="{{route('users-activity')}}">Пользователи</a>
            </div>

            <div id="right_menu" class="pull-right" style="margin-top: 10px;">
                <a class="btn btn-default" style="width: 100% !important;" href="{{ route('logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
            <div id="menu" class="pull-right" style="margin-top: 10px;">
                <a href="#" class="menu-trigger">Меню</a>
            </div>

            <div id="toogle_menu" class="pull-right" style="margin-top: 10px;">
                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Меню</a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{route('version.index')}}" style="width: 100% !important;">Версии</a>
                        </li>
                        <li>
                            <a href="{{route('users-activity')}}" style="width: 100% !important;">Пользователи</a>
                        </li>
                        <li>
                            <a style="width: 100% !important;" href="{{ route('logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
            </div>
    @endif
    <style>
        .btn-default.active {
            background: #991b1c !important;
            color: white;
        }
    </style>

</header>





