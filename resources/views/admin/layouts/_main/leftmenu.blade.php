<ul class="nav nav-list" style="top: 0px;">
    <li class="@if($active=='ios') active  @endif">
        <a href="/version" class="dropdown-toggle">
            <i class="menu-icon fa fa-server"></i>
            <span class="menu-text">Версии</span>
        </a>
    </li>
    <li class="@if($active=='user_activity') active  @endif">
        <a href="/users-activity" class="dropdown-toggle">
            <i class="menu-icon fa fa-user"></i>
            <span class="menu-text">Пользователи</span>
        </a>
    </li>
</ul>