@extends('admin.layouts.app')

@section('content')
    <div class="navbar navbar-default navbar-static-top" style="background: #af0606"; >
        <div class="col-xs-12"><h5 style="color: aliceblue; margin-top: 15px;
    margin-bottom: 15px;" class="center"><strong style="font-family: San Francisco;">Приложение ОЛИМП</strong></h5></div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-condensed">
                    <tbody>
                    <tr>
                        @forelse($versions as $version)
                            <td width="20%"><img src="/{{$version->image->path}}{{$version->image->name}}.xs.{{$version->image->extension}}"></td>
                            <td width="60%" height="60px" style="vertical-align: middle;><div style="word-break: break-all;">{{$version->name}}</div><br><div style="color: grey; margin-top: -3px; font-size: 11px;">{{$version->last_version}}</div><br>@if($version->active)<div style="font-size: 12px; color: #00CC33; margin-top: -21px;"><strong>Доступно</strong></div>@else<div style="color: red; margin-top: -21px; font-size: 12px;"><strong>Не доступно</strong></div>@endif</td>
            <td width="20%" style="vertical-align: middle; text-align: right;">@if($version->active)<a href="#" style="border-radius: 5px; padding: 0 10px; line-height: 28px; font-size: 0.900em; border: 1px solid #1e87f0; color: #1e87f0;" class="uk-button uk-button-default uk-button-large">Загрузить@endif</a></td>
            </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">нет версий</td>
                </tr>
                @endforelse
                </tbody>
                </table>
        </div>
    </div>
    </div>
@endsection

