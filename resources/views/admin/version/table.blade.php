<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
    <thead>
    <tr>
        <th data-hide="phone">ID</th>

        <th data-class="expand">App name</th>
        <th>Bundle id</th>
        <th data-hide="phone">Latest version</th>
    </tr>
    </thead>
    <tbody>

    @forelse($versions as $version)

        <tr>
            <td>{{$version->id}}</td>
            @if($version->active)
                <td><a id="link_mymodal2" class="link_mymodal2"

                       data-id="{{$version->id}}"
                       data-name="{{$version->name}}"
                       data-app_link="{{$version->app_link}}"
                       data-bundle_id="{{$version->bundle_id}}"
                       data-last_version="{{$version->last_version}}"
                       data-api_url="{{$version->api_url}}"
                       data-message_eng="{{$version->message_eng}}"
                       data-message_rus="{{$version->message_rus}}"
                       data-disallow_payments="{{$version->disallow_payments}}"
                       data-active="{{$version->active}}"
                       data-allow_countries="{{$version->countries}}"
                       data-hidden="{{$version->hidden}}"
                       data-description="{{$version->description}}">

                        {{$version->name}}
                    </a><i style="float: right;" class="fa fa-circle txt-color-green" aria-hidden="true"></i></td>
            @else
                <td><a id="link_mymodal2" class="link_mymodal2"


                       data-id="{{$version->id}}"
                       data-name="{{$version->name}}"
                       data-bundle_id="{{$version->bundle_id}}"
                       data-last_version="{{$version->last_version}}"
                       data-app_link="{{$version->app_link}}"
                       data-api_url="{{$version->api_url}}"
                       data-message_eng="{{$version->message_eng}}"
                       data-message_rus="{{$version->message_rus}}"
                       data-active="{{$version->active}}"
                       data-allow_countries="{{$version->countries}}"
                       data-hidden="{{$version->hidden}}"
                       data-disallow_payments="{{$version->disallow_payments}}"
                       data-description="{{$version->description}}">

                        {{$version->name}}
                    </a><i style="float: right;" class="fa fa-circle txt-color-red" aria-hidden="true"></i></td>
            @endif
            <td>{{$version->bundle_id}}</a></td>
            <td>{{$version->last_version}}</td>

        </tr>
    @empty
        <tr>
            <td colspan="5" class="text-center">Empty</td>
        </tr>

    @endforelse
    </tbody>
</table>