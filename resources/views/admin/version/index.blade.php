@extends('admin.layouts.app')

@section('title')
    Список версий
@stop

@section('content')

    @include("admin.layouts._main.header")

        <div class="content" style="margin: 10px;">
            <div class="row">
                <div class="col-lg-12 col-md-12">

                    <a class='btn btn-default pull-right' href="#myModal" style="float: left;" data-toggle="modal">Создать версию</a>

                    @include('admin.version.modal1')
                    @include('admin.version.modal2')

                    @include('admin.version.table')

                </div>
            </div>
        </div>

    @include("admin.layouts._main.footer")

@endsection

@section('scripts')
    @parent

    <script src="{{ asset('custom/js/dataTable.js') }}"></script>
    <script src="{{ asset('ace/js/duallistbox/ace.js') }}"></script>
    <script src="{{ asset('ace/js/duallistbox/elements.fileinput.js') }}"></script>
    <script src="{{ asset('ace/js/duallistbox/create.js') }}"></script>
    <script src="{{ asset('custom/duallistbox/dualbox_modal.js') }}"></script>

    <script>
        loadScript(('/smartadmin/js/table/jquery.dataTables.min.js'), function(){
            loadScript(("/smartadmin/js/table/dataTables.colVis.min.js"), function(){
                loadScript(("/smartadmin/js/table/dataTables.tableTools.min.js"), function(){
                    loadScript(("/smartadmin/js/table/dataTables.bootstrap.min.js"), function(){
                        loadScript(("/smartadmin/js/table/datatables.responsive.min.js"), pagefunction)
                    });
                });
            });
        });
    </script>

    <script>

        function new_tabs() {

            var ajax_countries = [];

            $.each($('#dual option:selected'), function(index, value) {
                ajax_countries.push({ index : value['value']});

            });
            console.log($('#id_option_tab').val());
            var id_tabs = document.getElementById('id_tabs');
            var id_tabs_var = id_tabs.value;

            var token = $('#token').attr('content');


            $.ajax({
                url: '{{ route('tabs-ajax') }}',
                type: 'post',
                data: {
                    'num_countries' : ajax_countries,
                    'name_tabs' : id_tabs_var,
                    '_token' : token
                },
                success: function (data) {
                    $('#id_tabs').val('');

                    var option_html = '<option id="id_option_tab" value="' + id_tabs_var + '">' + id_tabs_var + '</option>';
                    $('#id_select_tab').append(option_html);
                }
            });
        }
    </script>

    <script>

        $('#id_select_tab').change(function(){
            $('#show_countries').remove();

            var name_tab = $(this).val();
            var token = $('#token').attr('content');



            $.ajax({
                url: '{{ route('tabs_ajax_countries') }}',
                type: 'post',
                data: {
                    'name_tab' : name_tab,
                    '_token' : token
                },
                success: function (data) {

                    $.each($('#dual .dualclass'), function(index, value) {
                        data.db.forEach(function (element) {
                            if (element['name'] == value['value']) {
                                $('#downlist').append('<div id="show_countries"></div>')
                                $('#show_countries').append('<ul><li>' + $(value).text() + '</li></ul>');
                            }
                        })
                    });
                    $('#show_countries').append('<a onclick="change_countries_tabs();" href="javascript:void(0)">Активировать</a>');

                }
            });
        });

    </script>

    <script>

        function change_countries_tabs() {
            $.each($('#dual option:selected'), function(index, value) {
                $('.dualclass').attr('selected', false);
            });
            $('.demo2').bootstrapDualListbox('refresh');
            console.log($('#dual option:selected').text());

            var name_tab = $('#id_select_tab').val();
            var token = $('#token').attr('content');

            $.ajax({
                url: '{{ route('tabs_ajax_countries') }}',
                type: 'post',
                data: {
                    'name_tab' : name_tab,
                    '_token' : token
                },
                success: function (data) {
                    $.each($('#dual option:selected'), function(index, value) {
                        $('.dualclass').attr('selected', false);
                    });
                    $.each($('#dual .dualclass'), function(index, value) {
                        data.db.forEach(function (element) {
                            if (element['name'] == value['value']) {
                                $(value).attr('selected', 'selected');
                            } else {
                                $.each($('#dual option:selected'), function(index, value) {
                                    $('.dualclass').attr('selected', false);
                                });
                                $(value).attr('selected', 'selected');
                            }
                        })
                    });
                    $('.demo2').bootstrapDualListbox('refresh');
                }
            });
        }

    </script>

    <script>

        $('a[name=downlist]').click(function(){
            $('#downlist').slideToggle(500);
        });

    </script>

    <script>
        // дуалбокс модал 1
        var modal1 = $('.modal1').bootstrapDualListbox({
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true
        });
    </script>

    <script>
        function delete_tabs() {
            $.each($('#dual option:selected'), function(index, value) {
                $('.dualclass').attr('selected', false);
            });
            $('.demo2').bootstrapDualListbox('refresh');
        }
    </script>
@endsection
