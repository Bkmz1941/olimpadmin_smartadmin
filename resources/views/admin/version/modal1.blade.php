<div id="myModal" class="modal fade">
    <div class="container">
        <div class="row">

            <div id="modal" class="col-lg-12"><br>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Создать версию</h4>
                <hr>
                <form action="{{route('version.store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">

                        <!-- Первый столбик -->
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group{{ $errors->has('bundle_id') ? ' has-error' : '' }}">
                                <label>Bundle ID</label>
                                <input type="text" name="bundle_id" placeholder="bundle_id" class="form-control" value="{{ old('bundle_id') }}" autofocus required>
                                @if ($errors->has('bundle_id'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('bundle_id') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label>Название App</label>
                                <input type="text" name="name" placeholder="OLIMP Цупис" class="form-control" value="No Name">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('name') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('last_version') ? ' has-error' : '' }}">
                                <label>Последняя версия</label>
                                <input type="text" name="last_version" placeholder="3.2.1" class="form-control" value="1.2.3">
                                @if ($errors->has('last_version'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('last_version') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('api_url') ? ' has-error' : '' }}">
                                <label>Api адрес</label>
                                <input type="text" name="api_url" placeholder="https://api2.olimpapp.xyz:5500/api/" class="form-control">
                                @if ($errors->has('api_url'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('api_url') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('app_link') ? ' has-error' : '' }}">
                                <label>App Link</label>
                                <input type="text" name="app_link" placeholder="http://ya.ru" class="form-control" required>
                                @if ($errors->has('app_link'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('app_link') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>


                            <!-- ДУАЛЛИСТБОКС -->

                            <div class="form-group">
                                <div class="wrapp-dual{{ $errors->has('allow_countries') ? ' has-error' : '' }}">
                                    <label>Доступные страны</label>
                                    <select multiple="multiple" size="10" name="allow_countries[]" id="demo2" class="modal1">
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}} ({{$country->iso_code_2}})</option>
                                        @endforeach
                                    </select>
                                    <div class="err_dual"></div>
                                    @if ($errors->has('allow_countries'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('allow_countries') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <!-- ДУАЛЛИСТБОКС -->


                        </div>

                        <!-- Второй столбик -->
                        <div class="col-lg-6 col-md-6">
                            <div class="row">
                                <br>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                                    <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                        <div class="checkbox">
                                            <label>
                                                <input name="active" type="checkbox" class="ace" checked>
                                                <span class="lbl">Активна</span>
                                            </label>
                                        </div>
                                        @if ($errors->has('active'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('active') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="margin-top: 9px;">
                                    <div class="form-group{{ $errors->has('hidden') ? ' has-error' : '' }}">
                                        <div class="checkbox">
                                            <label>
                                                <input name="hidden" type="checkbox" class="ace">
                                                <span class="lbl"> Hidden</span>
                                            </label>
                                        </div>
                                        @if ($errors->has('hidden'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('hidden') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('message_eng') ? ' has-error' : '' }}">
                                <label>Сообщение на английском</label>
                                <input class="form-control" name="message_eng" value="The version of your application is not actual. Please update to the latest version.">
                                @if ($errors->has('message_eng'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('message_eng') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('message_rus') ? ' has-error' : '' }}">
                                <label>Сообщение на русском</label>
                                <input class="form-control" name="message_rus" value="Вышла новая версия. Пожалуйста, обновите приложение.">
                                @if ($errors->has('message_rus'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('message_rus') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label>Описание</label>
                                <input class="form-control" name="description" placeholder="description" required>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('description') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('disallow_payments') ? ' has-error' : '' }}">
                                <label>Disallow payments</label>
                                <textarea class="form-control" name="disallow_payments" placeholder="disallow_payments"></textarea>
                                @if ($errors->has('disallow_payments'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('disallow_payments') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-xs-12" style="margin-top: 26px;">
                                    <a class="btn btn-default active"class="btn btn-default active" href="javascript:void(0)" id="addParams">Добавить параметр</a>
                                </div>
                            </div>
                            <br>
                            <div class="wrapper-params-action"></div>

                        </div>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-primary  btn-block">Создать версию</button>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end -->