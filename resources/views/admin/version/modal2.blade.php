<div id="myModal2" class="modal fade" >
    {{csrf_field()}}
    <div class="container">
        <div class="row">
            <div id="modal" class="col-lg-12"><br>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Создать версию</h4>
                <hr>
                <form id="form_modal2" action="" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label>APP Name</label>
                                <input id="name_mod" type="text" name="name" placeholder="OLIMP Цупис" class="form-control" value="No Name" required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('name') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('bundle_id') ? ' has-error' : '' }}">
                                <label>Bundle ID</label>
                                <input id="bundle_id_mod" type="text" name="bundle_id" placeholder="bundle_id" class="form-control" value="" autofocus required>
                                @if ($errors->has('bundle_id'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('bundle_id') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('last_version') ? ' has-error' : '' }}">
                                <label>Latest Version</label>
                                <input id="last_version_mod" type="text" name="last_version" placeholder="3.2.1" class="form-control" value="1.2.3">
                                @if ($errors->has('last_version'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('last_version') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('api_url') ? ' has-error' : '' }}">
                                <label>Api Url</label>
                                <input id="api_url" type="text" name="api_url" placeholder="https://api2.olimpapp.xyz:5500/api/" class="form-control">
                                @if ($errors->has('api_url'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('api_url') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('app_link') ? ' has-error' : '' }}">
                                <label>App Link</label>
                                <input id="app_link_mod" type="text" name="app_link" placeholder="http://ya.ru" class="form-control">
                                @if ($errors->has('app_link'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('app_link') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>


                            <select multiple="multiple" size="10" name="allow_countries2[]" class="demo2" id="dual">
                                @foreach($countries as $country)
                                    <option class="dualclass" value="{{$country->id}}">
                                        {{$country->name}}
                                    </option>
                                @endforeach
                            </select>
                            <br>

                            <a class="btn btn-default active" name="downlist" href="javascript:void(0)">Шаблоны</a>
                            <br>
                                <div id="downlist" style="margin-top: 20px; box-shadow: 0px 0px 19px 6px #000000; background: grey; padding: 10px; display: none; width: 100%;">
                                    <div class="form-group">
                                            <label for="exampleFormControlSelect1">Создать шаблон</label><br>
                                        <input type="text" id="id_tabs">Название шаблона
                                        <br>
                                        <a href="javascript:void(0)" onclick="new_tabs();">Создать шаблон</a>
                                        <a href="javascript:void(0)" onclick="delete_tabs();">Очистить</a>
                                    </div>

                                    <br>

                                    <div class="form-group">
                                            <label for="exampleFormControlSelect1">Выберите шаблон</label>
                                        <select class="form-control" id="id_select_tab" name="" >
                                            @foreach($tabs as $tab)
                                                <option onclick="changes_tab_countries();" value="{{ $tab->name }}">{{ $tab->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div id="show_countries"></div>

                                </div>


                                <!--
                                <select id="id_select_tab" name="" >
                                    @foreach($tabs as $tab)
                                        <option onclick="changes_tab_countries();" value="{{ $tab->name }}">{{ $tab->name }}</option>
                                    @endforeach
                                </select>

                                    <br>
                                    <input type="text" id="id_tabs">Название шаблона
                                    <br>
                                    <a href="javascript:void(0)" onclick="new_tabs();">Создать шаблон</a>
                                -->
                        </div>

                        <!-- Второй столбик -->
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group{{ $errors->has('message_eng') ? ' has-error' : '' }}">
                                <label>English message</label>
                                <input id="message_eng" class="form-control" name="message_eng" value="The version of your application is not actual. Please update to the latest version.">
                                @if ($errors->has('message_eng'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('message_eng') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('message_rus') ? ' has-error' : '' }}">
                                <label>Russian message</label>
                                <input id="message_rus" class="form-control" name="message_rus" value="Вышла новая версия. Пожалуйста, обновите приложение.">
                                @if ($errors->has('message_rus'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('message_rus') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label>Description</label>
                                <input id="description" class="form-control" name="description" placeholder="description" required>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('description') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('disallow_payments') ? ' has-error' : '' }}">
                                <label>Disallow payments</label>
                                <textarea id="disallow_payments" class="form-control" name="disallow_payments" placeholder="disallow_payments"></textarea>
                                @if ($errors->has('disallow_payments'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('disallow_payments') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                                    <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                        <div class="checkbox">
                                            <label>
                                                <input id="active" name="active" type="checkbox" class="ace" checked>
                                                <span class="lbl"> Active</span>
                                            </label>
                                        </div>
                                        @if ($errors->has('active'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('active') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                                    <div class="form-group{{ $errors->has('hidden') ? ' has-error' : '' }}">
                                        <div class="checkbox">
                                            <label>
                                                <input id="active" name="hidden" type="checkbox" class="ace">
                                                <span class="lbl"> Hidden</span>
                                            </label>
                                        </div>
                                        @if ($errors->has('hidden'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('hidden') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12" style="margin-top: 30px;">
                                    <a class="btn btn-default active"href="javascript:void(0)" id="addParams2">Добавить параметр</a>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                    <div class="wrapper-params-action2"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <br><hr>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 3px !important;">
                            <div class="text-right">
                                <button class="btn btn-primary btn-block">Изменить</button>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 3px !important; padding-left: 3px !important;">
                            <a id="delete_id" href="" class="btn btn-danger btn-block">
                                Удалить
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 3px !important;">
                            <div class="text-right">
                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal" aria-hidden="true">Назад</button>
                            </div>
                        </div>
                    </div>


                    <br>
                </form>
                <form class="destroy_del" id="destroy_" method="POST">
                    {{ csrf_field() }}
                    {{method_field('DELETE')}}
                </form>
            </div>
        </div>
    </div>

</div>