@extends('admin.layouts.app')

@section('title')
    Авторизация
@endsection
@section('content')
    @include("admin.layouts._main.header")

    <div id="main" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-lg-offset-2 col-md-5 col-md-offset-2">
                    <div class="well no-padding">
                        <form action="{{ route('login') }}" id="login-form" class="smart-form client-form" novalidate="novalidate" method="post">
                            {{ csrf_field() }}
                            <header>
                                Авторизуйтесь
                            </header>

                            <fieldset>

                                <section>
                                    <label class="label">E-mail</label>
                                    <label class="input"> <i class="icon-append fa fa-user"></i>
                                        <input value="{{ old('email') }}" type="email" name="email" required>
                                        <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
                                </section>

                                <section>
                                    <label class="label">Password</label>
                                    <label class="input"> <i class="icon-append fa fa-lock"></i>
                                        <input type="password" name="password" required>
                                        <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
                                    <div class="note">
                                        <a href="#develop">Forgot password?</a>
                                    </div>
                                </section>

                                <section>
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember" checked="">
                                        <i></i>Запомнить</label>
                                </section>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">
                                    Авторизоваться
                                </ button>
                            </footer>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


@endsection
@section('scripts')
    @parent

@endsection
