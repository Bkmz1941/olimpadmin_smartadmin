$(document).ready(function () {

    $("#list").jqGrid({
        url: '/load-users',
        mtype: "GET",
        datatype: "json",
        colModel: [
            { label: 'User ID', name: 'user_id', key: true, width: 75 },
            { label: 'Last Activity', name: 'last_activity', width: 150 },
            { label: 'Email', name: 'email', width: 150 },
            { label: 'Login', name: 'login', width: 150 },
            { label:'Phone', name: 'phone', width: 150 }
        ],
        viewrecords: true,
        width: 780,
        height: 250,
        rowNum: 20,
        pager: "#jqGridPager"
    });

});