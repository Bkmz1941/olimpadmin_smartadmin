var thumb = $("#thumb");
thumb.ace_file_input({
    style: 'well',
    btn_choose: 'Image',
    btn_change: null,
    no_icon: 'ace-icon fa fa-image',
    droppable: true,
    thumbnail: 'large'//small | fit
});

thumb.ace_file_input('show_file_list', [
    {type: 'image', name: '', path: thumb_img}
]);


$("#params_copy").change(function(){
    var currentVal = $(this).val();
    if(currentVal != 0){
        $("#copyParams").prop('disabled', false);
    }else{
        $("#copyParams").prop('disabled', true);
    }

});

$("#copyParams").click(function(e){
    e.preventDefault();
    var version_id = $("#params_copy :selected").val();
    $.ajax({
        method:'get',
        url:'/version/copy/'+version_id,
        beforeSend: function(){
            $("#copyParams").prop('disabled', true);
        },
        success: function(response){
            if(response.status){
                $("#copyParams").prop('disabled', false);

                response.data.params.forEach(function(item, i, arr) {
                    copiedParams(item.param, item.value);
                });
            }
        }
    });
});

//$("#addParams").click(function(){
  //  addParams();
//});

$('body').on('click','#addParams', function(){
    console.log('123');
});

function copiedParams(param, value){

    var html = '<div class="currElem">'+
        '<div class="row">'+
        '<div class="col-sm-5 col-xs-12">'+
        '<label>Parameter</label>'+
        '<input type="text" name="param[]" class="form-control" placeholder="anyKey" value="'+param+'">'+
        '</div>'+
        '<div class="col-sm-5 col-xs-12">'+
        '<label>Value</label>'+
        '<input type="text" name="value[]" class="form-control" placeholder="anyValue" value="'+value+'">'+
        '</div>'+
        '<div class="col-sm-2 col-xs-12 text-center">'+
        '<label>Delete</label>'+
        '<div>'+
        '<a href="javascript:void(0)" onclick="deleteAttr(this)" class="removeElem">'+
        '<i class="fa fa-trash btn btn-danger btn-xs"></i>'+
        '</a>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>';

    $(".wrapper-params-action").append(html);
}

function addParams(){
    var html = '<div class="currElem">'+
        '<div class="row">'+
        '<div class="col-sm-5 col-xs-12">'+
        '<label>Parameter</label>'+
        '<input type="text" name="param[]" class="form-control" placeholder="anyKey">'+
        '</div>'+
        '<div class="col-sm-5 col-xs-12">'+
        '<label>Value</label>'+
        '<input type="text" name="value[]" class="form-control" placeholder="anyValue">'+
        '</div>'+
        '<div class="col-sm-2 col-xs-12 text-center">'+
        '<label>Delete</label>'+
        '<div>'+
        '<a href="javascript:void(0)" onclick="deleteAttr(this)" class="removeElem">'+
        '<i class="fa fa-trash btn btn-danger btn-xs"></i>'+
        '</a>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>';

    $(".wrapper-params-action").append(html);
}


function deleteAttr(elem){

    elem.closest('.currElem').remove();
}


var allow_countries = $('#allow_countries').bootstrapDualListbox({
    infoTextFiltered: '<span class="label label-purple label-lg">Отфильтровано</span>',
    infoText: '<span class="label_info">Страны</span> ({0})',
    infoTextEmpty: 'Пусто',
    moveAllLabel: 'Выбрать все',
    removeAllLabel: 'Удалить все'
});

var wrapper = allow_countries.bootstrapDualListbox('getContainer');
wrapper.find('.btn').addClass('btn-white btn-info btn-bold');
wrapper.find('.btn.btn-xs').html('все');
//wrapper.find('span.info').remove();
//wrapper.find('input.filter').remove();

$('#allow_countries').on('change',function(){
    $(".wrapp-dual .box2 .info .label_info").html('Страны');
});
