
$(document).ready(function () {

    $('#myModal2').on('hidden.bs.modal', function () {

        //location.href=location.href;

    });


    $('.link_mymodal2').click(function (e) {

        //Основные дынные
        var id = $(e.currentTarget).data('id');
        var name = $(e.currentTarget).data('name');
        var bundle_id = $(e.currentTarget).data('bundle_id');
        var app_link = $(e.currentTarget).data('app_link');
        var last_version = $(e.currentTarget).data('last_version');
        var api_url = $(e.currentTarget).data('api_url');
        var message_eng = $(e.currentTarget).data('message_eng');
        var message_rus = $(e.currentTarget).data('message_rus');
        var disallow_payments = $(e.currentTarget).data('disallow_payments');
        var active = $(e.currentTarget).data('active');
        var allow_countries = $(e.currentTarget).data('allow_countries');
        var hidden = $(e.currentTarget).data('hidden');
        var description = $(e.currentTarget).data('description');

        $('#name_mod').val(name);
        $('#bundle_id_mod').val(bundle_id);
        $('#last_version_mod').val(last_version);
        $('#app_link_mod').val(app_link);
        $('#api_url').val(api_url);
        $('#message_eng').val(message_eng);
        $('#message_rus').val(message_rus);
        $('#disallow_payments').val(disallow_payments);
        $('#description').val(description);
        if(active == 1) {
            $('#active').attr('checked', true);
        } else {
            $('#active').attr('checked', false);
        }

        // кнопка удалить
        $('#delete_id').attr('href', 'version/' + id );
        $('#delete_id').attr('onclick', 'event.preventDefault(); document.getElementById(\'destroy_'+id+'\').submit();');
        $('.destroy_del').attr('id', 'destroy_' + id);
        $('.destroy_del').attr('action', 'version/' + id);

        //кнопка изменить
        $('#form_modal2').attr('action', 'version/' + id);


        //bootstrapDualListbox
        $('#dual .dualclass').attr('selected', false);


        $.each($('#dual .dualclass'), function(index, value) {
            allow_countries.forEach(function (element) {
                if (element['id']==value['value']) {

                    $(value).attr('selected', true);
                }
            })
        });

        $('.demo2').bootstrapDualListbox('refresh');


        //Инициализация плагина bootstrapDualListbox
        var demo2 = $('.demo2').bootstrapDualListbox({
            nonSelectedListLabel: 'Non-selected',
            selectedListLabel: 'Selected',
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true
        });

        //Ajax на параметры
        var token = $('#token').attr('content');
        console.log(id);
        $.ajax ({
            url: 'key/' + id,
            type: 'post',
            data: {
                '_token' : token
            },
            success: function (data) {
                $('.currElem').remove();
                $.each(data , function(index, value) {
                    $.each(value, function (i, item) {

                        var html = '<div class="currElem">'+

                            '<div class="row">'+
                            '<div class="col-sm-5 col-xs-5 col-lg-5 col-md-5">'+
                            '<label>Parameter</label>'+
                            '<input type="text" name="param2[]" class="form-control" placeholder="anyKey" value="' + item['param'] + '">'+
                            '</div>'+
                            '<div class="col-sm-5 col-xs-5">'+
                            '<label>Value</label>'+
                            '<input type="text" name="value2[]" class="form-control" placeholder="anyValue" value="' + item['value'] + '">'+
                            '</div>'+
                            '<div class="col-sm-2 col-xs-1 text-center">'+
                            '<label>Delete</label>'+
                            '<div>'+
                            '<a href="javascript:void(0)" onclick="deleteAttr(this)" class="removeElem">'+
                            '<i class="fa fa-trash btn btn-danger btn-xs"></i>'+
                            '</a>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+

                            '</div>';

                        $(".wrapper-params-action2").append(html);
                    });
                });
            }
        });


        $('#myModal2').modal('show');

    })
});