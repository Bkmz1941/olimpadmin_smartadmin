<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Логин или пароль неверный',
    'throttle' => 'Много попыток входа, попробуйте через :seconds секунд.',
    'login_title' => 'Авторизация',
    'login_field' => 'Email или никнэйм',
    'password_field' => 'Пароль',
    'submit_btn' => 'Войти',
    'remember_me' => 'Запомнить меня',
    'register_link' => 'Регистрация',

    'register_title' => 'Регистрация',
    'register_email' => 'Email',
    'register_nickname' => 'Ник',
    'register_password' => 'Пароль',
    'register_repeat_password' => 'Повторите пароль',
];
