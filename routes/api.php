<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    Route::group(['namespace' => 'Api', 'prefix' => 'v1'], function() {

        Route::get('param', ['as' => 'api.version.code', 'uses' => 'ApiController@param']);
	Route::get('version', ['as' => 'api.version.code', 'uses' => 'ApiController@version']);
	Route::get('version/{id}', ['as' => 'api.version.bundle', 'uses' => 'ApiController@versionByBundleId']);
	Route::post('update_activity', ['as' => 'api.version.activity', 'uses' => 'ApiController@updateActivity']);
});

