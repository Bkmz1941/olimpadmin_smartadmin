<?php

Auth::routes();

Route::group(['namespace' => 'Front'],function() {

	Route::get('banner2',function(){
		return view('front.banner.index');
	});

	Route::get('banner',function(){
		return view('front.banner.index');
	});

	Route::get('/', ['as' => 'main','middleware' => 'checkClient','uses' => 'MainController@index']);
	Route::get('/ios', ['middleware' => 'isIos','as' => 'main.ios','uses' => 'MainController@ios']);
	Route::get('/android', ['middleware' => 'isAndroid','as' => 'main.android','uses' => 'MainController@android']);

});

Route::group(['middleware' => ['auth'], 'namespace' => 'Admin'],function() {

    Route::post('key/{id}','VersionControlController@key');
    Route::post('option/{id}','VersionControlController@option');
	Route::get('info','OlimpInfoController@index');
	Route::get('version/copy/{id}','VersionControlController@copyParams');
    Route::post('tabs_ajax_countries', 'VersionControlController@tabs_ajax_countries')->name('tabs_ajax_countries');
    Route::post('tabs_ajax', 'VersionControlController@tabs_ajax')->name('tabs-ajax');
	Route::get('users-activity-ajax', 'UsersActivityController@ajax')->name('users-activity-ajax');
	Route::get('users-activity','UsersActivityController@index')->name('users-activity');
	Route::get('load-users','UsersActivityController@loadUsers');
	Route::resource('version', 'VersionControlController');
	Route::resource('androidVersion', 'AndroidVersionController');
});

Route::group(['namespace' => 'Auth'], function() {

	Route::get('/','LoginController@showLoginForm');
});
