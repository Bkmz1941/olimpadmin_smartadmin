## OLIMP version control 

### To install:
1. composer install
2. php artisan migrate
3. php artisan db:seed
4. npm install
5. bower install
6. gulp

##API
  
**NAME**: Получить все активные версии  
**Method**: get  
**URL**: /api/v1/version    
**Return**: {'status','data'}  


**NAME**: Получить версию по bundle id  
**Method**: get  
**URL**: /api/v1/version/{id}    
**Return**: {'status','data'}  
**Description**: где {id} например com.arcanite.olimp3  


##Install Smart Banner

Need add between "head" tags code:  
```html
	<meta name="smartbanner:title" content="Olimp App">
    <meta name="smartbanner:author" content="Download our Official App">
    <meta name="smartbanner:price" content="FREE">
    <meta name="smartbanner:price-suffix-apple" content=" - On the App Store">
    <meta name="smartbanner:price-suffix-google" content=" - In Google Play">
    <meta name="smartbanner:icon-apple" content="https://olimpapp.info/img/logo_olimp.png">
    <meta name="smartbanner:icon-google" content="https://olimpapp.info/img/logo_olimp.png">
    <meta name="smartbanner:button" content="VIEW">
    <meta name="smartbanner:button-url-apple" content="https://olimpapp.info">
    <meta name="smartbanner:button-url-google" content="https://olimpapp.info">
    <meta name="smartbanner:enabled-platforms" content="android,ios">
    <link rel="stylesheet" href="https://olimpapp.info/js/smartbanner/dist/smartbanner.css">
    <script src="https://olimpapp.info/js/smartbanner/dist/smartbanner.js"></script>
```
