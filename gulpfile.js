var gulp = require('gulp');

gulp.task('js_config', function(){
    return gulp.src('./resources/assets/libs/smartadmin/js/*.js')
        .pipe(gulp.dest('./public/smartadmin/js/'));
});
gulp.task('bootstrap', function(){
    return gulp.src('./resources/assets/libs/smartadmin/js/bootstrap/*.js')
        .pipe(gulp.dest('./public/smartadmin/js/bootstrap/'));
});
gulp.task('libs', function(){
    return gulp.src('./resources/assets/libs/smartadmin/js/libs/*.js')
        .pipe(gulp.dest('./public/smartadmin/js/libs/'));
});
gulp.task('dropzone', function(){
    return gulp.src('./resources/assets/libs/smartadmin/js/plugin/dropzone/*.js')
        .pipe(gulp.dest('./public/smartadmin/js/dropzone/'));
});
gulp.task('css', function(){
    return gulp.src('./resources/assets/libs/smartadmin/css/*.css')
        .pipe(gulp.dest('./public/smartadmin/css/'));
});
gulp.task('img', function(){
    return gulp.src('./resources/assets/libs/smartadmin/img/*')
        .pipe(gulp.dest('./public/smartadmin/img/'));
});
gulp.task('fonts', function(){
    return gulp.src(['./resources/assets/libs/smartadmin/fonts/*', './resources/assets/libs/smartadmin/fonts/**/*'])
        .pipe(gulp.dest('./public/smartadmin/fonts/'));
});
gulp.task('database', function(){
    return gulp.src(['./resources/assets/libs/smartadmin/js/datatable-responsive/*.js', './resources/assets/libs/smartadmin/js/datatables/*.js', './resources/assets/libs/smartadmin/js/delete-table-row/*.js'])
        .pipe(gulp.dest('./public/smartadmin/js/table/'));
});
gulp.task('ace_duallistbox_js', function(){
    return gulp.src(['./resources/assets/libs/ace/assets/js/ace.js', './resources/assets/libs/ace/assets/js/duallistbox.min.js', './resources/assets/custom/js/admin/version/create.js', './resources/assets/libs/ace/assets/js/src/elements.fileinput.js'])
        .pipe(gulp.dest('./public/ace/js/duallistbox/'));
});
gulp.task('ace_duallistbox_css', function(){
    return gulp.src('./resources/assets/libs/ace/assets/css/duallist.min.css')
        .pipe(gulp.dest('./public/ace/css/duallistbox/'));
});
gulp.task('custom_duallistbox', function(){
    return gulp.src(['./resources/assets/custom/duallistbox/dualbox_modal.js', './resources/assets/custom/duallistbox/jquery.bootstrap-duallistbox.js', './resources/assets/custom/duallistbox/bootstrap-duallistbox.css'])
        .pipe(gulp.dest('./public/custom/duallistbox/'));
});
gulp.task('vue', function(){
    return gulp.src(['./resources/assets/custom/js/Custom_Vue.js', './resources/assets/libs/vue/vue.js'])
        .pipe(gulp.dest('./public/custom/vue/'));
});
gulp.task('dataTable', function(){
    return gulp.src(['./resources/assets/custom/js/dataTable.js', './resources/assets/custom/js/dataTableUsers.js'])
        .pipe(gulp.dest('./public/custom/js/'));
});

gulp.task('default', ['dataTable', 'vue', 'custom_duallistbox', 'dropzone', 'libs', 'bootstrap', 'js_config', 'css', 'fonts', 'img', 'database', 'ace_duallistbox_js', 'ace_duallistbox_css']);
